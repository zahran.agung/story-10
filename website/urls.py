from django.urls import path, include
from .views import SignUp
from django.views.generic.base import TemplateView

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('', TemplateView.as_view(template_name='index.html'), name='index'),
    path('signup/', SignUp.as_view(), name='signup')
]