from django.test import TestCase,Client
from django.urls import resolve
from .views import SignUp

# Create your tests here.

class UnitTest(TestCase):
    def test_main_url_exists(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)
